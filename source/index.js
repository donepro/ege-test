import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import 'normalize.css';
import 'wicg-focus-ring';

import App from './containers/App';
// import {getData} from './actions';
// import data from './data/data.json';

import store from './store';

// store.dispatch(getData(data));

ReactDOM.render(
    <Provider store={store}>
      <App/>
    </Provider>,
    document.getElementById('root')
);


