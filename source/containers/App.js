import React from 'react';
import styled, {injectGlobal} from 'styled-components';
import {connect} from 'react-redux';
import {HashRouter as Router, Route, Switch, Link} from 'react-router-dom';

// eslint-disable-next-line
injectGlobal`
  @font-face {
    font-family: "Helvetica";
    src:
    url("fonts/Helvetica-Regular.woff2") format("woff2"),
    url("fonts/Helvetica-Regular.woff") format("woff");
    font-weight: 400;
    font-style: normal;
  }

  @font-face {
    font-family: "Helvetica";
    src:
    url("fonts/Helvetica-Light.woff2") format("woff2"),
    url("fonts/Helvetica-Light.woff") format("woff");
    font-weight: 300;
    font-style: normal;
  }

  body {
    font-family: "Helvetica", "Arial", sans-serif;
    font-size: 16px;
    line-height: 25px;
  }

  .js-focus-ring :focus:not(.focus-ring) {
    outline: 0;
  }
`;

const Test = () => {
  return (
    <div>Страница теста</div>
  );
};

const NotFound = () => {
  return (
    <div>Страница не найдена 404</div>
  );
};


const Home = () => {
  return (
    <div>
      <Link to="/test/123">Начать тест</Link>
    </div>
  );
};

const App = () => {

  return (
    <Router>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/:subject/:variant" component={Test} />
        <Route component={NotFound} />
      </Switch>
    </Router>
  );

};

const mapStateToProps = (state) => {
  return {
  };
};

export default connect(mapStateToProps)(App);
