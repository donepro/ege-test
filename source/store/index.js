import logger from 'redux-logger';
import thunk from 'redux-thunk';
import {createStore, applyMiddleware} from 'redux';

import reducer from '../reducers';
import initialState from './state';

const middlewares = [thunk];

if (process.env.NODE_ENV !== 'production') {
  middlewares.push(logger);
}

const store = createStore(reducer, initialState, applyMiddleware(...middlewares));

export default store;
